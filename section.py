
class Section:
    def __init__(self):
        self.prompts = {}
        self.last_prompt = ''
        self.name = ''

#TODO find a consistent naming pattern for the keys
class DirectLabor:
    def __init__(self):
        self.prompts = {
            #'table': 'Yrs Fringe',
            'Direct Labor': 'header',
            'Category\nDescription\nEducation' : 'table',
            'Are the labor rates detailed below fully loaded?' : 'fully_loaded',
            'Please explain any costs that apply.': 'explanation',
            'Provide any additional information and cost support data related to the nature of the direct labor detailed above.': 'additional_information',
            'Sum of all Direct Labor Costs is($):' : 'sum_costs'
        }

        self.last_prompt = 'Sum of all Direct Labor Costs is($):'
        self.name = 'direct_labor'

    def populate(self):
        pass

class Overhead:
    def __init__(self):
        self.prompts = {
            'Overhead': 'header',
            'Labor Cost Overhead Rate': 'labor_overhead',
            'Material Cost Overhead Rate': 'material_overhead',
            'Other Direct Costs Overhead': 'odc_overhead',
            'Overhead Comments:': 'comments',
            'Overhead Cost ($):': 'sum_overhead'
        }
        self.last_prompt = 'Overhead Cost ($):' 
        self.name = 'overhead'

class GnA:
    def __init__(self):
        self.prompts = {
            'General and Administration Cost': 'header',
            'G&A Rate (%):': 'gna_rate',
            'Apply G&A Rate to Overhead Costs?': 'gna_overhead',
            'Apply G&A Rate to Direct Labor Costs?': 'gna_dl',
            'Apply G&A Rate to Direct Material Costs?': 'gna_dm',
            'Apply G&A Rate to ODC- Supply?': 'gna_odc_supply',
            'Apply G&A Rate to ODC- Equipment?': 'gna_odc_equipment',
            'Apply G&A Rate to ODC- Travel?': 'gna_odc_travel',
            'Apply G&A Rate to Other Direct Costs?': 'gna_odc',
            "Please specify the different cost sources below from which your company's General and Administrative costs are calculated.": 'gna_details',
            'G&A Cost ($):': 'gna_cost' 
        }
        self.last_prompt = 'G&A Cost ($):'
        self.name = 'general_and_administrative'

class Subcontractor:
    def __init__(self):
        self.prompts = {
            'Subcontractor/Consultants': 'header',
            'Subcontractor/Consultant:': 'contractor',
            'Do you have a letter of commitment from the subcontractor/consultant?': 'letter_of_commitment',
            'Provide an explanation of any contact you have had with the subcontractor/consultant as to their availability to perform the proposed work.': 'explanation',
            "Should the G&A rate for this proposal's budget include the cost for this subcontractor/consultant?": 'gna_rate_applies',
            'Are you able to provide detailed budget information for this subcontractor/consultant?': 'budget_details',
            'Total Cost($) :': 'total_costs',
            'Do you provide the authority to the Government to contact this Budget Contact?': 'contact_permission'
        }
        self.last_prompt = 'Do you provide the authority to the Government to contact this Budget Contact?'
        self.name = 'subcontractor'

class ODCMaterials:
    def __init__(self):
        self.prompts = {
            'ODC-Materials': 'header',
            'Description:': 'description',
            'Supporting Comments:': 'comments'
        }
        self.last_prompt = 'Supporting Comments:'
        self.name = 'odc_materials'
 
class ODCSupplies:
    def __init__(self):
        self.prompts = {
            'ODC-Supplies': 'header',
            'Description:': 'description',
            'Supporting Comments:': 'comments'
        }
        self.last_prompt = 'Supporting Comments:'
        self.name = 'odc_supplies'

class ODCEquipment:
    def __init__(self):
        self.prompts = {
            'ODC-Equipment': 'header',
            'Description:': 'description',
            'Supporting Comments:': 'comments'
        }
        self.last_prompt = 'Supporting Comments:'
        self.name = 'odc_equipment'

class ODCTravel:
    def __init__(self):
        #so we're just going to drop this here for now and collect all the data about each trip as a blob
        #TODO figure out how to parse the travel data
        #also this section is definitely bugged. it writes out travel purpose twice
        self.prompts = {
            'ODC-Travel': 'header',
            'Description:' : 'description',
            'Total Costs ($):' : 'total_costs',#we can probably grab the total and remove de second purpose, but lets save tha for later
            'Sources of Estimates:': 'estimate_source',
            'Explanation/Justifications:': 'justification' 
        }
        self.last_prompt = 'Explanation/Justifications:'
        self.name = 'odc_travel'

class ODCOther:
    def __init__(self):
        self.prompts = {
            'ODC-Other': 'header',
            'Description:': 'description',
            'Supporting Comments:': 'comments'
        }
        self.last_prompt = 'Supporting Comments:'
        self.name = 'odc_other'
       
class ODCSummary:
    def __init__(self):
        self.prompts = {
            'ODC-Summary': 'header',
            'ODC-Materials': 'odc_materials',
            'ODC-Supplies': 'odc_supplies',
            'ODC-Equipment': 'odc_equipment',
            'ODC-Travel': 'odc_travel',
            'ODC-Other': 'odc_other',
            'Do you have any additional information to provide?': 'additional_information'
        }
        self.last_prompt = 'Do you have any additional information to provide?'
        self.name = 'odc_summary'

class ProfitRate:
    def __init__(self):
        self.prompts = {
            'Profit Rate/Cost Sharing': 'header',
            'Cost Sharing ($):': 'cost_sharing',
            'Cost Sharing Explanation:': 'cost_sharing_explanation',
            'Profit Rate (%):': 'profit_rate',
            'Profit Explanation:': 'profit_explanation',
            'Total Profit Cost ($):': 'total_profit_cost',
            'Total Proposed Amount ($):': 'total_proposed_amount'
        }
        self.last_prompt = 'Total Profit Cost ($):'
        self.name = 'profit_rate'