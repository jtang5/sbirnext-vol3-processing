import textract
import camelot
import re
import json
import pandas as pd
from tabula import read_pdf
from pathlib import Path
from section import DirectLabor, Overhead, GnA, Subcontractor, ODCMaterials, ODCEquipment, ODCSupplies, ODCTravel, ODCOther, ODCSummary, ProfitRate


table_boundary = 'Are the labor rates detailed below fully loaded?'
headers = [
            'Direct Labor\n\n',
            'Overhead\n\n', 
            'General and Administration Cost\n\n',
            'Subcontractor/Consultants\n\n',
            'ODC-Materials\n\n', #Other Direct Costs
            'ODC-Travel\n\n',
            'ODC-Summary\n\n',
            'Profit Rate/Cost Sharing\n\n' 
        ]

section_patterns = {
    'direct_labor' : re.compile(r'Direct Labor\n(.*?)Overhead\nBase\n', re.DOTALL),
    'overhead': re.compile(r'Overhead\n(.*?)General and Administration Cost\n', re.DOTALL),
    'g&a': re.compile(r'General and Administration Cost\n(.*?)Subcontractor/Consultants\n', re.DOTALL),
    'subcontractors': re.compile(r'Subcontractor/Consultants\n(.*?)ODC-Materials\n', re.DOTALL),
    'odc_materials': re.compile(r'ODC-Materials\n(.*?)ODC-Travel\n', re.DOTALL),
    'odc_travel': re.compile(r'ODC-Travel\n(.*?)ODC-Summary\n', re.DOTALL),
    'odc_summary': re.compile(r'ODC-Summary\n(.*?)Profit Rate/Cost Sharing\n', re.DOTALL),
    'profit': re.compile(r'Profit Rate/Cost Sharing\n(.*?)$', re.DOTALL) 
}

#the first one or two prompts of each section to enable the switch
switch_prompts = {
    'Direct Labor': DirectLabor(),
    'Are the labor rates detailed below fully loaded?': DirectLabor(),
    'Overhead': Overhead(),
    'Labor Cost Overhead Rate': Overhead(),
    'General and Administration Cost': GnA(),
    'G&A Rate (%):': GnA(),
    'Subcontractor/Consultants': Subcontractor(),
    'Subcontractor/Consultant:': Subcontractor(),
    'ODC-Materials': ODCMaterials(),
    'ODC-Equipment': ODCEquipment(),
    'ODC-Supplies': ODCSupplies(),
    'ODC-Travel': ODCTravel(),
    'ODC-Other': ODCOther(),
    'ODC-Summary': ODCSummary(),
    'Profit Rate/Cost Sharing': ProfitRate(),
    'Cost Sharing ($):' : ProfitRate(),
    'Cost Sharing Explanation:' : ProfitRate(),
    'Profit Rate (%):' : ProfitRate(),
    'Profit Explanation:': ProfitRate(),
    'Total Profit Cost ($):': ProfitRate()
}

subheaders = [
    'Base',
    'Year2'
]

direct_labor_template = {
    #subheader,
    #table,
    'Are the labor rates detailed below fully loaded?',
    'Provide any additional information and cost support data related to the nature of the direct labor \n\ndetailed above.',
    'Sum of all Direct Labor Costs is($):'

}

def load_data(data_path):
    for f in data_path.glob('*/*Vol3*'):
        if f.is_file():
            print(f)
            #text = textract.process(str(f))
            #pdfminer = textract.process(str(f), method='pdfminer') #pdfminer seems to be the most reliable of the bunch
            #tesseract = textract.process(str(f), method='tesseract', language='eng')
            #tab = read_pdf(str(f))
            #lat_tables = camelot.read_pdf(str(f), pages='all')
            #tables = camelot.read_pdf(str(f), pages='all', flavor='stream')            
            #import pdb; pdb.set_trace()
            try:
                parse_vol3(str(f))
            except:
                pass

def parse_vol3(vol3_path):
    #text = textract.process(vol3_path)
    #text = textract.process(vol3_path, method='pdfminer') #pdfminer seems to be the most reliable of the bunch
    #text = textract.process(vol3_path, method='tesseract', language='eng')
    #text = text.decode('utf-8')
    #search = re.search(r'Direct Labor\n\n(.*?)Overhead\n\n',text, re.DOTALL)
    #tables = camelot.read_pdf(vol3_path, pages='all')
    #for now the table are assumed to go in order i.e. the first one will always succeed, the second might not and not vice versa
    lat_tables = camelot.read_pdf(vol3_path, pages='all')
    tables = camelot.read_pdf(vol3_path, pages='all', flavor='stream', edge_tol=2000)#, row_tol=20, column_tol=10) 
    #sections = separate_sections(section_patterns, text)
    frames = [table.df for table in tables]
    tables = pd.concat(frames)
    result = parse_table(tables)
    #import pdb; pdb.set_trace()
    dfs = []
    for table in lat_tables:
        dfs.append(first_row_headers(table.df))

    #import pdb; pdb.set_trace()
    try:
       result['direct_labor']['Base']['table'] =  dfs[0].to_dict()
       result['direct_labor']['Year2']['table'] =  dfs[1].to_dict()
    except Exception:
       pass
       #import pdb; pdb.set_trace()
    vol_name = vol3_path.split('/')[-1]
    vol_name = vol_name.replace('.pdf', '.json')
    #create output path if not exist
    Path('./output').mkdir(parents=True, exist_ok=True)
    out_path = Path('./output/'+ vol_name)
    with open(out_path, 'w') as out:
        out.write(json.dumps(result, indent=2))
    


#gonna have to refactor so much of this. Probably use the visitor pattern. Good enough for now though
def parse_table(table):
    result = {}
    possible_prompt = ''
    current_prompt = ''
    section = None
    next_section = None
    section_set = False
    next_subheader = 'Base'
    current_subheader = 'Base'
    collected = ' '
    for index, row in table.iterrows():
        #match row to delimiter 
        start_row = row[0]
        rest_row = row[1:]
        #import pdb; pdb.set_trace()

        two_line_prompt = ' '.join([possible_prompt, start_row]) #maybe we should have the join delim be \n 
        
        #find the correct section
        #if not section_set:
        if start_row in switch_prompts:
            if not section:
                section = switch_prompts[start_row]
            else:
                next_section = switch_prompts[start_row]
            section_set = True
        elif two_line_prompt in switch_prompts:
            if not section:
                section = switch_prompts[start_row]
            else:
                next_section = switch_prompts[two_line_prompt]
            section_set = True
            if section_set:
                if section.name not in result:
                    next_subheader = 'Base'
        #process the current state
        if start_row in subheaders:
            next_subheader = start_row

        if section:
            #import pdb; pdb.set_trace()
            if start_row in subheaders:
                if current_prompt:
                    check_result_structure(result, section.name, current_subheader)
                    result[section.name][current_subheader][section.prompts[current_prompt]] = collected
                current_prompt = ''
                collected = ''
            elif start_row in section.prompts or (next_section and start_row in next_section.prompts):
                if current_prompt:
                    check_result_structure(result, section.name, current_subheader)
                    result[section.name][current_subheader][section.prompts[current_prompt]] = collected
                current_prompt = start_row  
                collected = clean_row(rest_row)
                if next_subheader != current_subheader:
                    current_subheader = next_subheader
                if next_section and next_section != section:
                    section = next_section
            elif two_line_prompt in section.prompts or (next_section and start_row in next_section.prompts):
                collected = collected.replace(possible_prompt,'')
                collected = collected.strip()
                if current_prompt:
                    check_result_structure(result, section.name, current_subheader)
                    result[section.name][current_subheader][section.prompts[current_prompt]] = collected
                current_prompt = two_line_prompt
                collected = clean_row(rest_row)
                if next_subheader != current_subheader:
                    current_subheader = next_subheader
                if next_section and next_section != section:
                    section = next_section
            else:
                collected += ' ' + clean_row(row)
                possible_prompt = clean_row(row)
            collected = collected.strip()
            possible_prompt = possible_prompt.strip()
            # switch sections when we reach the end of the section
            if current_prompt == section.last_prompt:
                section_set = False

    #import pdb; pdb.set_trace()  
    check_result_structure(result, section.name, current_subheader)
    result[section.name][current_subheader][section.prompts[current_prompt]] = collected
    
    return result


def clean_row(row):
    cleaned = [nan_check(item) for item in row]
    return ' '.join(cleaned)

def nan_check(item):
    item = str(item)
    return item.replace('nan', '')

def check_result_structure(result, header, subheader):
    if not header in result:
        result[header] = {}
    if not subheader in result[header]:
        result[header][subheader] = {}

def separate_sections(section_patterns, text):
    sections = {}
    for section, pattern in section_patterns.items():
        hmm = pattern.search(text)
        import pdb; pdb.set_trace()
        sections[section] = pattern.search(text).group(0)
    return sections
        


def first_row_headers(df):
    new_header = df.iloc[0]
    df = df[1:]
    df.columns = new_header
    return df

if __name__ == '__main__':
    load_data(Path('data'))
